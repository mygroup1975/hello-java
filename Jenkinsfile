def GIT_REPO = 'https://gitlab.com/mygroup1975/hello-java'
def BRANCH = 'main'
def COV_URL = 'https://testing.coverity.synopsys.com'
def COV_PROJECT = 'vivek-hello-java'
def COV_STREAM = "$COV_PROJECT-$BRANCH"
def CHECKERS = '--webapp-security --enable-callgraph-metrics'
def BLDCMD = 'mvn -B package -DskipTests'
def COVBIN = '/opt/coverity/analysis/2022.6.1/bin'

pipeline {
    agent any
    tools {
        maven 'maven-3.8'
        //jdk 'openjdk-11'
    }
    stages {
        stage('checkout') {
            steps {
                cleanWs()
                git url: "$GIT_REPO", branch: "$BRANCH", credentialsId: 'gitlab-vkallank'
            }
        }
        stage('coverity') {
            steps {
                withCredentials([usernamePassword(credentialsId: 'coverity-committer', usernameVariable: 'COV_USER', passwordVariable: 'COVERITY_PASSPHRASE')]) {
                    sh """
                        export PATH=$PATH:$COVBIN
                        export COVERITY_NO_LOG_ENVIRONMENT_VARIABLES=1
                        cov-build --dir idir --fs-capture-search $WORKSPACE $BLDCMD
                        cov-analyze --dir idir --ticker-mode none --strip-path $WORKSPACE $CHECKERS
                        cov-commit-defects --dir idir --ticker-mode none --url $COV_URL --stream $COV_STREAM \
                            --description $BUILD_TAG --version \$(git log -n 1 --pretty=format:%H)
                        curl -fLsS --user $COV_USER:$COVERITY_PASSPHRASE $COV_URL/api/viewContents/issues/v1/Outstanding%20Issues?projectId=$COV_PROJECT | \
                            tee results.json | python3 -m json.tool
                    """
                }
            }
        }
    }
    post {
        always {
            archiveArtifacts artifacts: 'idir/build-log.txt, idir/output/analysis-log.txt, idir/output/callgraph-metrics.csv'
            cleanWs()
        }
    }
}
